/*
Make structs that use functions (method-like)

*/



#include <stdio.h>
/* Function ptrs with structure */

struct temp 
{
    /*
    f is an array of 3 pointers
    to functions returning a void
    */
    int age;    
    
    void (*f[3])(); //three positions array
    
    int (*large[1])(int x, int y); //one position array
    

};


// FUNCTION DECLARATION

void f1();
void f2();
void f3();
    
int larger(int x, int y)
{
	if (x >y)
	{
		return x;
	}
	else
	{
		return y;
	}
}
    
    
    
    
    
//MAIN
int main() 
{
#if 0
	struct temp t;
	
	t.f[0] = f1;
	t.f[1] = f2;
	t.f[2] = f3;
	
	t.large[0] = larger;
	
	t.age = 10;
	
#endif


    struct temp t={10, {f1,f2,f3}, larger};

 
    for (int i=0;  i<3;  ++i) t.f[i]();    
    return 0;


}//END OF MAIN


void f1() 
{
    printf("How ");  return;
}


void f2() 
{
    printf("are ");  return;
}


void f3() 
{
    printf("you?\n");  return;
}








