#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>//for uint8_t etc

int main()
{
    printf("Hello world!\n");

printf("============================================Simple Typedef + Pointer initialize & use\n\r");
    /*
    Here, we are defining a type defSimpleType. Variables of this type point to
    functions, that take an integer as argument and does not return any value.
    */
    typedef void
    (*defSimpleType) (int error_no, void *user_data);

    /*
    Here, myDefSimpleType is a variable of type defSimpleType, which can point to any function
    (aka address) like defSimpleTypeUse that takes in an int as input and does not return any value.
    */
    defSimpleType myDefSimpleType;

    //We restrict defSimpleTypeUse in here, in this scope.
    {
        //A function that takes in an int as input and does not return any value
        void defSimpleTypeUse (int errorNo, void *userData)
        {
            printf("defSimpleTypeUse: We can call defSimpleTypeUse outside of its scope!.\n\r");
            printf("errorNo=%d, userData after=%d \n\r",errorNo,userData);
        }
        //We can use myDefSimpleType now to access/use/call the function defSimpleTypeUse from anywhere in
        //the code!
        myDefSimpleType=defSimpleTypeUse;

    }
    //Without the myDefSimpleType, we could not use defSimpleTypeUse out here!

    int someData=11;
    printf("someData value before:%d \n\r",someData);
    void *aPointer;
    aPointer = &someData;
    aPointer = 12;

    myDefSimpleType(1234, aPointer);

//============================================Complex Typedef


    return 0;
}
