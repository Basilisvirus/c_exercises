
//C code test

#include <stdio.h>
#include <stdlib.h>


int n, *ptr;
char temp;


//Person structure
struct person 
{
	const char *name;
	int age;
	void (*functions[3]) ();
};


//FOR PERSON: Increase age by 1
void birthday(person *p)
{
	(*p).age++;
}


//FUNCTION struct;

struct function
{
	int age;
	
	int (*fun[3])(int x, int y);
	
	void (*fun_raw[0])(int x, int y);
};


//FOR FUNCTION
int add (int x, int y)
{
	return x+y;
}

int sub (int x, int y)
{
	return x-y;
}

int mul (int x, int y)
{
	return x*y;
}

void larger (int x, int y)
{
	if(x>y)
	{
		printf("x = %i is larger\n", x);
	}
	else 
	{
		printf("y = %i is larger\n", y);
	}
	
	
}


int main()
{


	struct function temp;
	
	temp.age = 20;
	temp.fun[0] = add;
	temp.fun[1] = sub;
	temp.fun[2] = mul;
	
	temp.fun_raw[0] = larger;


	temp.fun_raw[0](2, 1);

#if 0
	//=======================================================Printf & Pointers======================================

	//printf( "Hello \n");


	const char *name; // = "John";
	name = "John";

	printf("*name= %c", *name); //J
	printf("\n");

	printf("&name= %p", &name);//0x00FH
	printf("\n");

	printf("name= %s",name);//John
	printf("\n");

	printf("============================\n");

	
	//======================================================More pointers===========================================

	char vowels[] = {'A', 'E', 'I', 'O', 'U'};
	char *pvowels ; //= &vowels[0];
	pvowels = vowels; //or pvowels = &vowels[0];
	int i;

	printf("*pvowels: 	%c \n",*(pvowels)); //Α
	printf("pvowels[0]: 	%c \n", pvowels[0]); //A

	printf("pvowels: 	%p \n", pvowels); //0x00F
	printf("&pvowels[0]: 	%p \n", &pvowels[0]); //0x00F

	//printf("*pvowels[0]: 	%u \n", *pvowels[0] ); //Non existent
	//printf("&pvowels: 	%p \n", &pvowels+1); // ??

	printf("============================\n");

	printf("*vowels: 	%c \n", *(vowels)); // A
	printf("vowels[0]: 	%c \n", vowels[0]); // A


	printf("vowels: 	%p \n", vowels); //0x00F
	printf("&vowels[0]: 	%p \n", &vowels[0]); //0x00F

	//printf("*vowels[0]: 	%p \n", *vowels[0]); //Non existent
	//printf("&vowels: 	%p \n", &vowels+1); //??


	

	person Vasilis;

	Vasilis.name = "Vasilis";
	Vasilis.age = 23;

	printf("Vasilis.name is %s\n", Vasilis.name);
	printf("Vasilis.age is %i\n", Vasilis.age);

	//=========================================function arguments by reference======================================
	
	
	birthday (&Vasilis);
	printf("Vasilis.age after birthday is %i\n", Vasilis.age);
	
	//=========================================Memory dynamic allocation============================================
	printf("press enter ");
	getchar();


	printf("Enter number of elements ");
	scanf("%d", &n);

	ptr = (int*) malloc(n* sizeof(int));

	if(ptr== NULL){
		printf("Not enough memory\n");
	}
	else{
		printf("All good\n");
	}

	//Print current memory blocks used 
	for (int i=0; i<=(n-1); i++){
		printf("%u \n", (ptr + i));
	}


	//free the memory	
	free(ptr);
	
	
#endif




}//END OF MAIN












