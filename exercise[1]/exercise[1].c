/*
Write a C program that will take as input the numbers 10, 11, 12, 13, 14, 15 as an array, and the (output 1) will be an
applied square to these numbers, (function int square) and the the squared numbers will be checked wether they are even
or not (function int is_even). If a number is even, the (output 2) will be 1, if not, the output will be 0.
Print those numbers, as shown below:

Example:

actual array (input)
  10   11   12   13   14   15
applied square() (output 1)
 100  121  144  169  196  225
applied is_even() (output 2)
   1    0    1    0    1    0

all arrays are created as pointers.
in order to apply functions square(), is_even(), a int * map(int (*function)(int), int *array, int length ) 
function should be used to apply each function to the arrays.
*/


//used for ptintf
#include <stdio.h>

/*INPUT: a function to use (on every element of the array, on an int array and its length. OUTPUT: the return of the new
 temp_arr created.
*/
int * map (int (*function)(int), int *array, int length)
{
	int *temp_arr = new int[length];
	
	length = (length -1);
	
	
	for(int i=0; i<=length; i ++ )
	{
		*(temp_arr+i) = function( *(array+i) );
	}
	
	return temp_arr;
}

//INPUT: int number. OUTPUT: square of this number
int square (int value)
{
	return value*value;
}

//INPUT: int number. OUTPUT: 1 if the input is even, 0 if input is not even.
int is_even (int value)
{
	return !(value%2);
}


//INPUT: An array and the size of this array. OUTPUT: printf of the array, and \n
void print (int *array, int length)
{	
	length = (length -1);
	
	for (int i=0; i<=length; i++)
	{
		printf("%5i", *(array+i) );	
	}
	
	printf("\n");
}


int main(int argc, char *argv[])
{

	//making a pointer to an array of 6-position int
	int *arr1 = new int[6];

	
	//fill the array
	for (int i=10; i<=15; i++)
	{
		*(arr1 + (i-10)) = i; 		//put the numbers inside the array
	}//END OF FOR

	
	//print the original array
	printf("original array is: \n");
	print(arr1, 6);
		
	//make array for the squared values
	int *arr2 = new int[6];
	
	//calculate and return the square of the array, into new array. The arr1 wont be affected.
	arr2 = map(square, arr1, 6);
	
	//print the new squared array
	printf("squared values are: \n");	
	print(arr2, 6);
	
	//create final array (will hold is_even results)
	int *arr3 = new int[6];
	
	//calculate the is_even for every element
	arr3 = map(is_even, arr2, 6);
	
	//print the results
	printf("applying is_even: \n");
	print(arr3, 6);
	
}//END OF MAIN

















